
from django.urls import path

from api.views import ProcessData

urlpatterns = [
    path('data/', ProcessData,),
]